#include "PlayScene.h"
#include "Player.h"
#include "Engine/Model.h"
//コンストラクタ
Player::Player(GameObject* parent)
    :GameObject(parent, "Player")
{
    int hModel_ = -1;
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
    hModel_ = Model::Load("Player.fbx");
    assert(hModel_ >= 0);


}

//更新
void Player::Update()
{
}

//描画
void Player::Draw()
{
}

//開放
void Player::Release()
{
}
